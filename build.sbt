name := "megafon-task"
version := "0.1"

scalaVersion := "2.12.8"

val sparkVersion = "2.4.2"

libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-mllib" % sparkVersion

libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test"