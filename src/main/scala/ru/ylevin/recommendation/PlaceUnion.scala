package ru.ylevin.recommendation

import scala.collection.mutable

/**
  * Объединение дубликатов мест.
  * В случае когда набор дубликатов состоит из одного элемента - это просто место без дубликатов
  * @param elems набор дубликатов
  * @param average среднее значение координат мест
  */
private[recommendation]
case class PlaceUnion private(elems: List[Place], average: Coordinate) extends Serializable {
    val ids: Set[Long] = elems.map(_.id).toSet

    def union(geoGrid: GeoGrid, other: PlaceUnion): PlaceUnion = {
        val map = mutable.Map[Long, Place]()
        (elems.toStream ++ other.elems).foreach(place => {
            map(place.id) = place
        })

        val all = map.values

        new PlaceUnion(all.toList, geoGrid.average(all.view.map(_.coordinate)))
    }

    def merge(): Place = {
        val first = elems.minBy(_.id)
        val date = elems.map(_.date).minBy(_.getMillis)

        Place(
            first.id,
            first.name,
            first.category,
            first.description,
            average,
            first.regionId,
            date
        )
    }
}

object PlaceUnion {
    def apply(place: Place): PlaceUnion = new PlaceUnion(List(place), place.coordinate)
}