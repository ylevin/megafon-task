package ru.ylevin.recommendation

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

case class Coordinate(latitude: Double, longitude: Double) extends Serializable

case class Place(id: Long,
                 name: String,
                 category: String,
                 description: String,
                 coordinate: Coordinate,
                 regionId: Int,
                 date: DateTime,
                ) extends Serializable

case class Visit(personId: Long,
                 time: DateTime,
                 coordinate: Coordinate,
                 regionId: Int,
                 date: DateTime,
                ) extends Serializable

case class PersonPlace(personId: Long,
                       placeId: Long) extends Serializable

case class Recommendation(personId: Long,
                          placeId: Long,
                          rating: Double,
                          placeName: String,
                          description: String,
                          coordinate: Coordinate,
                          regionId: Int,
                          date: DateTime,
                         ) extends Serializable

object Date {
    private val formatter = DateTimeFormat.forPattern("yyyMMdd")

    def parse(str: String): DateTime = {
        formatter.parseDateTime(str)
    }

    def apply(year: Int, month: Int, day: Int): DateTime =
        new DateTime(year, month, day, 0, 0)
}

object Time {
    private val formatter = DateTimeFormat.forPattern("yyyMMdd_HH")

    def parse(str: String): DateTime = {
        formatter.parseDateTime(str)
    }

    def apply(year: Int, month: Int, day: Int, hour: Int): DateTime =
        new DateTime(year, month, day, hour, 0)
}