package ru.ylevin.recommendation

import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.rdd.RDD

class Calculator(private val distanceThreshold: Double,
                 private val levenshteinThreshold: Int,
                 private val rank: Int,
                 private val maxVisitCount: Int,
                ) extends Serializable {

    /**
      * Вычисление рекоммендаций
      * @param places набор мест
      * @param visits набор пребываний пользователей
      * @return список рекоммендаций мест для пользователей
      */
    def calculateRecommendations(places: RDD[Place], visits: RDD[Visit]): RDD[Recommendation] = {
        // выбираем первое место для каждого региона и считаем это координату опорной
        // для вычисления координатной сетки для региона
        val samples = places
                .keyBy(_.regionId)
                .reduceByKey { case (p, _) => p }
                .mapValues(_.coordinate)
                .collectAsMap()


        val geoCalculator = new GeoCalculator(
            new FlatGeoGrid.Set(samples, distanceThreshold),
            distanceThreshold,
            levenshteinThreshold
        )

        // удаляем дубликаты мест
        val deduplicatedPlaces = geoCalculator.removeDuplicates(places).cache()

        // считаем количество посещений
        val counts = geoCalculator.countVisits(deduplicatedPlaces, visits).cache()

        // превращаем информацию о количестве посещений в рейтинги
        val ratings = counts.map { case (PersonPlace(personId, placeId), count) =>
            ALS.Rating(personId, placeId, Math.min(count, maxVisitCount).toFloat / maxVisitCount)
        }

        val (personsFactors, placesFactors) = ALS.train(ratings, rank)

        val placeById = deduplicatedPlaces.map(place => (place.id, place))

        geoCalculator.getToEstimate(deduplicatedPlaces, visits, counts.map(_._1))
                .map(p => (p.placeId, p))
                .join(placesFactors)
                .map { case (_, (p, placeFactors)) =>
                    (p.personId, (p, placeFactors))
                }
                .join(personsFactors)
                .map { case (_, ((p, placeFactors), personFactors)) =>
                    (p, (personFactors, placeFactors))
                }
                // вычисляем рейтинг рекоммендации на основе матрицы пользователей и матрицы мест
                .map { case (PersonPlace(personId, placeId), (personFactors, placeFactors)) =>
                    val rating = personFactors.zip(placeFactors)
                            .map { case (a, b) => a * b }
                            .sum

                    (placeId, (personId, rating))
                }
                .join(placeById)
                // заполняем данные мест
                .map { case (_, ((personId, rating), place)) =>
                    Recommendation(
                        personId = personId,
                        placeId = place.id,
                        rating = rating,
                        placeName = place.name,
                        description = place.description,
                        coordinate = place.coordinate,
                        regionId = place.regionId,
                        date = place.date)
                }
    }
}