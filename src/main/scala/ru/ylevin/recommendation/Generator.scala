package ru.ylevin.recommendation

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.joda.time.DateTime

import scala.collection.mutable
import scala.util.Random

class Generator(val categoryCount: Int = 10,
                val categoryNameLength: Int = 10,
                val placeNameLength: Int = 10,
                val descriptionLength: Int = 20,
                val maxShiftFromCenter: Int = 15000,
                val personsCount: Int = 20000,
                val visitsCount: Int = 1000000,
                val placesCount: Int = 5000,
                val minDate: DateTime = new DateTime(2010, 1, 1, 0, 0),
                val maxDate: DateTime = new DateTime(2019, 4, 1, 0, 0),
                val regionCenters: Seq[Coordinate]) {

    private val minHour = (minDate.getMillis / Generator.millisInHour).toInt

    private val maxHour = (maxDate.getMillis / Generator.millisInHour).toInt

    private val rangeHour = maxHour - minHour

    private val categories = Array.fill(categoryCount) {
        Random.nextString(categoryNameLength)
    }

    private val cache = mutable.Map[Coordinate, FlatGeoGrid]()

    private var placeIdCounter = 0L

    private def nextDate(): DateTime =
        new DateTime((minHour + Random.nextInt(rangeHour)) * Generator.millisInHour)

    private def nextCoordinate(regionId: Int): Coordinate = {
        val center = regionCenters(regionId)
        cache.getOrElseUpdate(center, {
            FlatGeoGrid(center, 1.0)
        }).shift(center,
            Random.nextDouble() * maxShiftFromCenter,
            Random.nextDouble() * maxShiftFromCenter)
    }

    private def nextPlace(): Place = {
        val regionId = Random.nextInt(regionCenters.size)

        val coordinate = nextCoordinate(regionId)

        val name = Random.nextString(placeNameLength)

        val category = categories(Random.nextInt(categories.length))

        val description = Random.nextString(descriptionLength)

        placeIdCounter += 1

        val date = nextDate().withTimeAtStartOfDay()

        Place(placeIdCounter, name, category, description, coordinate, regionId, date)
    }

    private def nextVisit(): Visit = {
        val regionId = Random.nextInt(regionCenters.size)

        val coordinate = nextCoordinate(regionId)

        val personId = Random.nextInt(personsCount)

        val time = nextDate()

        Visit(
            personId,
            time,
            coordinate,
            regionId,
            time.withTimeAtStartOfDay()
        )
    }

    def createVisitRdd(sc: SparkContext): RDD[Visit] = {
        sc.makeRDD(Stream.fill(visitsCount) { nextVisit() })
    }

    def createPlaceRdd(sc: SparkContext): RDD[Place] = {
        sc.makeRDD(Stream.fill(placesCount) { nextPlace() })
    }
}

object Generator {
    val millisInHour: Long = 1000 * 60 * 60

    val mscCoord = Coordinate(55.75, 37.62)
    val spbCoord = Coordinate(59.89, 30.26)
    val srtCoord = Coordinate(51.54, 46.01)
    val vldCoord = Coordinate(43.12, 131.92)

    def main(args: Array[String]): Unit = {
        val spark = SparkSession
                .builder()
                .master("local")
                .appName("generator")
                .getOrCreate()

        val sc = spark.sparkContext

        val generator = new Generator(regionCenters = Seq(mscCoord, spbCoord, srtCoord, vldCoord))

        generator.createVisitRdd(sc).saveAsObjectFile("visits")
        generator.createPlaceRdd(sc).saveAsObjectFile("places")
    }
}
