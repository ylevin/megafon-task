package ru.ylevin.recommendation

import org.apache.spark.sql.SparkSession

object App {
    def main(args: Array[String]): Unit = {
        val spark = SparkSession
                .builder()
                .master("local")
                .appName("recommendation calculator")
                .getOrCreate()

        val sc = spark.sparkContext

        val places = sc.objectFile[Place]("places").cache()
        val visits = sc.objectFile[Visit]("visits").cache()

        val calculator = new Calculator(
            distanceThreshold = 100,
            levenshteinThreshold = 2,
            rank = 10,
            maxVisitCount = 5
        )

        calculator.calculateRecommendations(places, visits)
                .saveAsObjectFile("recommendation")
    }
}
