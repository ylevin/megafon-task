package ru.ylevin.recommendation

import org.apache.spark.rdd.RDD
import spire.std.LevenshteinDistance

import scala.collection.mutable

private[recommendation]
class GeoCalculator(private val gridSet: GeoGridSet,
                    private val distanceThreshold: Double,
                    private val levenshteinThreshold: Double,
                   ) extends Serializable {
    /**
      * @param places множество мест
      * @param visits множество пребываний пользователей
      * @param matched пары человек-место, для которых верно, что человек был в этом месте хотя бы раз
      * @return множество пар человек-место для которых нужно вычислить рекоммендации.
      */
    def getToEstimate(places: RDD[Place], visits: RDD[Visit], matched: RDD[PersonPlace]): RDD[PersonPlace] = {
        val placesRegions = places
                .map(place => (place.regionId, place.id))

        val personsRegions = visits
                .map(visit => (visit.regionId, visit.personId))
                .distinct()

        val possibleMatches = placesRegions.join(personsRegions)
                .values
                .map { case (placeId, personId) => PersonPlace(personId, placeId) }

        possibleMatches.map((_, null))
                .leftOuterJoin(matched.map((_, null)))
                .filter(_._2._2.isEmpty)
                .keys
    }

    /**
      * Удаляет дубликаты мест на основании местоположения и похожести имен
      * @param places исходный набор мест
      * @return набор мест без дубликатов
      */
    def removeDuplicates(places: RDD[Place]): RDD[Place] = {
        var unionPlaces = places.map(PlaceUnion(_))

        gridSet.gridCaseNum.foreach(i => {
            unionPlaces = unionPlaces.map(place => (keysOf(place)(i), place))
                    .groupByKey()
                    .mapValues(unionSimilarPlaces)
                    .flatMap(_._2)
        })

        unionPlaces.map(_.merge())
    }

    /**
      * Вычисляет сколько раз в каком месте побывал человек
      * @param deduplicatedPlaces множество мест
      * @param visits множество пребываний пользователей
      * @return пара состоящая из объекта пользователь-место и количества пребываний
      *         Возвращаются только те пары для которых колчество пребываний не равно 0
      */
    def countVisits(deduplicatedPlaces: RDD[Place], visits: RDD[Visit]): RDD[(PersonPlace, Int)] = {
        val placePairs = deduplicatedPlaces.flatMap(place =>
            keysOf(place).map((_, place))
        )

        val visitPairs = visits.flatMap(visit =>
            keysOf(visit).map((_, visit))
        )

        // объединяем места и пребывания по ключу ячеек
        placePairs.join(visitPairs).values
                // фильтруем по пороговому расстоянию
                .filter { case (place, visit) => matchVisit(visit, place) }
                // группируем по пребыванию
                .keyBy { case (_, visit) => (visit.personId, visit.time) }
                .groupByKey()
                // находим ближайшее место для каждого пребывания
                .mapValues(findNearestVisits)
                .values.keyBy { case (place, visit) => PersonPlace(visit.personId, place.id) }
                // считаем количество пребываний
                .map(entry => (entry._1, 1))
                .reduceByKey(_ + _)
    }

    private def matchVisit(visit: Visit, place: Place): Boolean = {
        val distance = gridSet.gridForRegion(place.regionId).distance(place.coordinate, visit.coordinate)
        distance <= distanceThreshold && place.date.compareTo(visit.time) <= 0
    }

    /**
      * Находит среди множеста мест и пребываний ближайшие друг к другу
      * @param pairs пары место-пребывание
      * @return наиболее подходящая пара
      */
    private def findNearestVisits(pairs: Iterable[(Place, Visit)]): (Place, Visit) =
        pairs.minBy { case (place, visit) =>
            gridSet.gridForRegion(place.regionId).distance(place.coordinate, visit.coordinate)
        }

    private def unionSimilarPlaces(places: Iterable[PlaceUnion]): Seq[PlaceUnion] = {
        val array = places.toArray

        val geoGrid = gridSet.gridForRegion(places.head.elems.head.regionId)

        val links = Array.fill(array.length) {
            mutable.Set[Int]()
        }

        for (i <- array.indices) {
            for (j <- 0 until i) {
                if (isOverlapped(geoGrid, array(i), array(j))) {
                    links(i) += j
                    links(j) += i
                }
            }
        }

        Utils.getComponents(links.map(_.toSet))
                .map(set => set
                        .map(array(_))
                        .reduce(_.union(geoGrid, _))
                )
    }

    /**
      * @param geoGrid географическая сетка для региона, в котором проверяются дубликаты мест
      * @param placeUnion1 первое объединение дубликатов (или место)
      * @param placeUnion2 второе объединение дубликатов (или место)
      * @return нужно ли объединения дубликатов сливать вместе
      */
    private def isOverlapped(geoGrid: GeoGrid, placeUnion1: PlaceUnion, placeUnion2: PlaceUnion): Boolean = {
        if (placeUnion1.ids.intersect(placeUnion2.ids).nonEmpty) {
            return true
        }

        if (geoGrid.distance(placeUnion1.average, placeUnion2.average) > distanceThreshold) {
            return false
        }

        for (place1 <- placeUnion1.elems; place2 <- placeUnion2.elems) {
            if (LevenshteinDistance.distance(place1.name, place2.name) <= levenshteinThreshold) {
                return true
            }
        }

        false
    }

    /**
      * Вычисляет набор ключей ячеек для координаты по каждому из возможных разбиений
      * @param region регион
      * @param coordinate координаты
      * @return список ключей ячеек
      */
    private def keysOf(region: Int, coordinate: Coordinate): Seq[GeoKey] =
        gridSet.gridCaseNum.map(i =>
            GeoKey(region, gridSet.gridForRegion(region).cellOf(coordinate, i))
        )

    private def keysOf(visit: Visit): Seq[GeoKey] =
        keysOf(visit.regionId, visit.coordinate)

    private def keysOf(place: Place): Seq[GeoKey] =
        keysOf(place.regionId, place.coordinate)

    private def keysOf(place: PlaceUnion): Seq[GeoKey] =
        keysOf(place.elems.head.regionId, place.average)

}
