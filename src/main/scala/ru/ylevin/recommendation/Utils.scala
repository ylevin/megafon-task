package ru.ylevin.recommendation

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Utils {
    /**
      * Вычисляет набор связных компонент неориентированного графа
      * @param links массив где i-й элемент - множество соединений для i-й вершины
      * @return набор компонент
      */
    def getComponents(links: Array[Set[Int]]): List[Set[Int]] = {
        val result = ListBuffer[Set[Int]]()

        val visited = Array.fill(links.length)(false)

        for (i <- links.indices) {
            if (!visited(i)) {
                val component = mutable.Set[Int](i)
                val newNodes = mutable.Set[Int]()
                component ++= links(i)
                do {
                    newNodes.clear()
                    for (j <- component if !visited(j)) {
                        visited(j) = true
                        newNodes ++= links(j)
                    }
                    component ++= newNodes
                } while (newNodes.nonEmpty)
                result += component.toSet
            }
        }

        result.toList
    }
}
