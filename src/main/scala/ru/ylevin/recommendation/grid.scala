package ru.ylevin.recommendation

/**
  * Ключ для разбиения по географической сетки по ячейкам
  * Используется для группирования по координатам
  * для дальнейшей проверки близости координат
  * @param x номер ячейки по широте
  * @param y номер ячейки по долготе
  * @param n номер разбиения
  */
case class GeoCell(x: Long, y: Long, n: Int) extends Serializable

/**
  * Ключ ячейки с указанием региона
  */
case class GeoKey(regionId: Int, cell: GeoCell) extends Serializable

/**
  * Абстракция для представления географической сетки
  */
trait GeoGrid extends Serializable {
    /**
      * Возвращает ключ ячейки для координаты
      * @param coordinate координаты
      * @param caseI номер разбиения
      * @return ключ ячейки
      */
    def cellOf(coordinate: Coordinate, caseI: Int): GeoCell

    def distance(coordinate1: Coordinate, coordinate2: Coordinate): Double

    /**
      * Сдвинуть координату
      * @param coordinate исходная координата
      * @param x количество метров по широте
      * @param y количество метров по долготе
      * @return координата со сдвигом
      */
    def shift(coordinate: Coordinate, x: Double, y: Double): Coordinate

    /**
      * Вычисляет среднее координат
      * @param coordinates множество координат
      * @return среднее значение координат
      */
    def average(coordinates: Iterable[Coordinate]): Coordinate
}

/**
  * Плоская географическая сетки, используется для упрощения вычисления расстояний
  * по координатам в одном регионе
  *
  * @param latitudeMultiplier  значение на которое нужно умножить разницу в широте
  *                            чтобы получить расстояние в метрах
  *
  * @param longitudeMultiplier значение на которое нужно умножить разницу в долготе
  *                            чтобы получить расстояние в метрах
  *
  * @param gridSize            размер стороны сетки (в метрах)
  */
class FlatGeoGrid private[recommendation](val latitudeMultiplier: Double,
                                          val longitudeMultiplier: Double,
                                          val gridSize: Double) extends GeoGrid {
    @transient
    private val latitudeGridSize = gridSize / latitudeMultiplier

    @transient
    private val longitudeGridSize = gridSize / longitudeMultiplier

    override def cellOf(coordinate: Coordinate, caseI: Int): GeoCell = {
        val x = round((caseI & 1) == 1)(coordinate.latitude / latitudeGridSize)
        val y = round((caseI & 2) == 2)(coordinate.longitude / longitudeGridSize)
        GeoCell(x, y, caseI)
    }

    override def distance(coordinate1: Coordinate, coordinate2: Coordinate): Double = {
        val dx = (coordinate2.latitude - coordinate1.latitude) * latitudeMultiplier
        val dy = (coordinate2.longitude - coordinate1.longitude) * longitudeMultiplier
        Math.sqrt(dx * dx + dy * dy)
    }

    override def shift(coordinate: Coordinate, x: Double, y: Double): Coordinate = {
        Coordinate(
            coordinate.latitude + x / latitudeMultiplier,
            coordinate.longitude + y / longitudeMultiplier
        )
    }

    override def average(coordinates: Iterable[Coordinate]): Coordinate = {
        var lon = 0.0
        var lat = 0.0

        coordinates.foreach(coord => {
            lon += coord.longitude
            lat += coord.latitude
        })

        Coordinate(lat / coordinates.size, lon / coordinates.size)
    }

    private def round(shifted: Boolean = true): Double => Long =
        if (shifted)
            d => Math.round(d)
        else
            d => Math.floor(d).toLong
}

/**
  * Абстракция для множества разных сеток для каждого региона
  */
trait GeoGridSet extends Serializable {
    /**
      * @return Количество разбиений сетки
      */
    def gridCaseNum: Range

    /**
      * Сетка для региона
      * @param regionId регион
      * @return географическая сетка
      */
    def gridForRegion(regionId: Int): GeoGrid
}

object FlatGeoGrid {
    private val EARTH_RADIUS_METERS = 6371000

    private val THRESHOLD_MULTIPLIER = 2.1

    def gridCaseNum: Range = 0 to 3

    def apply(estimate: Coordinate, distanceThreshold: Double): FlatGeoGrid = {
        val longitudeMultiplier = EARTH_RADIUS_METERS * Math.abs(Math.cos(estimate.latitude))
        val latitudeMultiplier = EARTH_RADIUS_METERS
        val gridSize = distanceThreshold * THRESHOLD_MULTIPLIER
        new FlatGeoGrid(latitudeMultiplier, longitudeMultiplier, gridSize)
    }

    class Set(samples: collection.Map[Int, Coordinate], distanceThreshold: Double) extends GeoGridSet {
        private val grids = samples
                .mapValues(FlatGeoGrid(_, distanceThreshold))
                .map(identity)

        override def gridCaseNum: Range = FlatGeoGrid.gridCaseNum

        override def gridForRegion(regionId: Int): GeoGrid = grids(regionId)
    }
}
