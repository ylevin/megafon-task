package ru.ylevin.recommendation

import org.scalatest.FunSuite

import scala.util.Random

class FlatGeoGridCellsTest extends FunSuite {
    private val seed = 100
    private val count = 1000

    private val min = 0.0
    private val max = 2.0

    private val threshold = 0.5

    private val random = new Random(seed)

    test("Match Coords Cells Test") {
        val geoGrid = new FlatGeoGrid(1.0, 1.0, threshold * 2)

        (1 to count) foreach { _ =>
            val (first, second) = nextPair(min, max)
            val matchCount = FlatGeoGrid.gridCaseNum.count(i =>
                geoGrid.cellOf(first, i) == geoGrid.cellOf(second, i)
            )
            assert(matchCount > 0)
        }
    }

    def nextPair(min: Double, max: Double): (Coordinate, Coordinate) = {
        val first = Coordinate(nextDouble(min, max), nextDouble(min, max))
        val angle = nextDouble(0, Math.PI * 2)
        val distance = nextDouble(0, threshold)
        val second = Coordinate(
            first.latitude + Math.cos(angle) * distance,
            first.longitude + Math.sin(angle) * distance
        )
        (first, second)
    }

    def nextDouble(min: Double, max: Double): Double = {
        min + random.nextDouble() * (max - min)
    }
}
