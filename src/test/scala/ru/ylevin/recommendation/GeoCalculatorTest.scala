package ru.ylevin.recommendation

trait GeoCalculatorTest extends SparkTest {
    protected val calculator = new GeoCalculator(
        FakeGeoGridSet,
        distanceThreshold = 0.5,
        levenshteinThreshold = 1,
    )
}
