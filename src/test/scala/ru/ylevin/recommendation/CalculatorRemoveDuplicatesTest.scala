package ru.ylevin.recommendation

import org.scalatest.FunSuite

class CalculatorRemoveDuplicatesTest extends FunSuite with GeoCalculatorTest {
    test("No Data Test") {
        val places = sc.makeRDD(Seq[Place]())

        assertResult(Array()) {
            calculator.removeDuplicates(places).collect()
        }
    }

    test("One Place Test") {
        val place = Place(1, "AAA", "A", "AAA", Coordinate(0.1, 0.9), 1, Date(2019, 1, 9))

        val places = sc.makeRDD(Seq(place))

        assertResult(Array(place)) {
            calculator.removeDuplicates(places).collect()
        }
    }

    test("Simple Test") {
        val places = sc.makeRDD(Seq(
            Place(1, "AAA", "A", "AAA", Coordinate(0.1, 0.9), 1, Date(2019, 1, 9)),
            Place(2, "AAA", "A", "AAA", Coordinate(0.3, 0.9), 1, Date(2019, 1, 2)),
            Place(3, "AAA", "A", "AAA", Coordinate(0.2, 1.2), 1, Date(2019, 2, 9)),
            Place(4, "BBB", "A", "AAA", Coordinate(0.2, 1.2), 1, Date(2018, 11, 19)),
        ))

        val deduplicated = calculator.removeDuplicates(places).collect()

        assert(deduplicated.length == 2)

        assert(deduplicated(0).id == 1)
        assert(deduplicated(1).id == 4)

        assert(deduplicated(0).name == "AAA")
        assert(deduplicated(1).name == "BBB")

        assert(deduplicated(0).date == Date(2019, 1, 2))
        assert(deduplicated(1).date == Date(2018, 11, 19))

        assertEqualsDouble(0.2, deduplicated(0).coordinate.latitude)
        assertEqualsDouble(1.0, deduplicated(0).coordinate.longitude)

        assertEqualsDouble(0.2, deduplicated(1).coordinate.latitude)
        assertEqualsDouble(1.2, deduplicated(1).coordinate.longitude)
    }

    def assertEqualsDouble(expected: Double, actual: Double, accuracy: Double = 1e-5): Unit = {
        assert(Math.abs(expected - actual) <= accuracy, s"expected: $expected but actual: $actual")
    }
}
