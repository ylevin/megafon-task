package ru.ylevin.recommendation

import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

trait SparkTest {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark test")
      .getOrCreate()
  }

  def sc: SparkContext = spark.sparkContext
}