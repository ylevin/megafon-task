package ru.ylevin.recommendation

import org.scalatest.FunSuite

class CalculatorCountVisitsTest extends FunSuite with GeoCalculatorTest {
    test("Single Visit Test") {
        val places = sc.makeRDD(Seq(
            Place(1, "AAA", "A", "", Coordinate(0.1, 0.9), 1, Date(2019, 1, 9)),
            Place(2, "BBB", "B", "", Coordinate(0.2, 1.2), 1, Date(2019, 1, 9)),
        ))

        val visits = sc.makeRDD(Seq(
            Visit(1, Time(2019, 2, 10, 12), Coordinate(0.2, 1.2), 1, Date(2019, 2, 10)), // Place 2
        ))

        assertResult(Array((PersonPlace(1, 2), 1))) {
            calculator.countVisits(places, visits).collect()
        }
    }

    test("No Match Test") {
        val places = sc.makeRDD(Seq(
            Place(1, "AAA", "A", "", Coordinate(0.1, 0.9), 1, Date(2019, 1, 9)),
            Place(2, "BBB", "B", "", Coordinate(0.2, 1.2), 1, Date(2019, 1, 9)),
        ))

        val visits = sc.makeRDD(Seq(
            Visit(1, Time(2019, 2, 10, 12), Coordinate(0.0, 2.0), 1, Date(2019, 2, 10)),
        ))

        assertResult(Array()) {
            calculator.countVisits(places, visits).collect()
        }
    }

    test("Two Visits Test") {
        val places = sc.makeRDD(Seq(
            Place(1, "AAA", "A", "", Coordinate(0.1, 0.9), 1, Date(2019, 1, 9)),
            Place(2, "BBB", "B", "", Coordinate(0.2, 1.2), 1, Date(2019, 1, 9)),
        ))

        val visits = sc.makeRDD(Seq(
            Visit(1, Time(2019, 2, 10, 12), Coordinate(0.1, 0.9), 1, Date(2019, 2, 10)), // Place 1
            Visit(1, Time(2019, 2, 11, 12), Coordinate(0.1, 0.9), 1, Date(2019, 2, 11)), // Place 1
        ))

        assertResult(Array((PersonPlace(1, 1), 2))) {
            calculator.countVisits(places, visits).collect()
        }
    }

    test("Match Date Test") {
        val places = sc.makeRDD(Seq(
            Place(1, "AAA", "A", "", Coordinate(0.1, 0.9), 1, Date(2019, 1, 9)),
            Place(2, "BBB", "B", "", Coordinate(0.2, 1.2), 1, Date(2019, 2, 19)),
            Place(3, "CCC", "C", "", Coordinate(1.1, 0.2), 1, Date(2019, 2, 1)),
            Place(4, "DDD", "D", "", Coordinate(1.5, 0.1), 1, Date(2019, 2, 15)),
        ))

        val visits = sc.makeRDD(Seq(
            Visit(1, Time(2019, 2, 10, 12), Coordinate(0.2, 1.2), 1, Date(2019, 2, 10)), // Place 1
            Visit(1, Time(2019, 2, 20, 10), Coordinate(0.1, 0.8), 1, Date(2019, 2, 20)), // Place 1
            Visit(1, Time(2019, 2, 20, 12), Coordinate(0.2, 1.2), 1, Date(2019, 2, 20)), // Place 2
        ))

        assertResult(Array((PersonPlace(1, 1), 2), (PersonPlace(1, 2), 1))) {
            calculator.countVisits(places, visits).collect()
        }
    }
}
