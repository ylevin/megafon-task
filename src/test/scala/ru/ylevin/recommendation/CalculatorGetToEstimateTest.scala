package ru.ylevin.recommendation

import org.scalatest.FunSuite

class CalculatorGetToEstimateTest extends FunSuite with GeoCalculatorTest {
    test("Simple Test") {
        val places = sc.makeRDD(Seq(
            Place(1, "AAA", "A", "", Coordinate(0.1, 0.9), 1, Date(2019, 1, 9)),
            Place(2, "BBB", "B", "", Coordinate(0.2, 1.2), 1, Date(2019, 2, 19)),
            Place(3, "CCC", "C", "", Coordinate(1.1, 0.2), 1, Date(2019, 2, 1)),
            Place(4, "DDD", "D", "", Coordinate(1.5, 0.1), 1, Date(2019, 2, 15)),
            Place(5, "EEE", "E", "", Coordinate(1.1, 0.1), 2, Date(2019, 1, 10)),
            Place(6, "FFF", "E", "", Coordinate(1.1, 1.0), 2, Date(2019, 1, 10)),
        ))

        val visits = sc.makeRDD(Seq(
            Visit(1, Time(2019, 2, 10, 12), Coordinate(0.2, 1.2), 1, Date(2019, 2, 10)), // Place 1
            Visit(1, Time(2019, 2, 20, 10), Coordinate(0.1, 0.8), 1, Date(2019, 2, 20)), // Place 1
            Visit(1, Time(2019, 2, 20, 12), Coordinate(0.2, 1.2), 1, Date(2019, 2, 20)), // Place 2
        ))

        val matched = sc.makeRDD(Seq(
            PersonPlace(1, 1),
            PersonPlace(1, 2)
        ))

        assertResult(Set(PersonPlace(1, 3), PersonPlace(1, 4))) {
            calculator.getToEstimate(places, visits, matched).collect().toSet
        }
    }
}
