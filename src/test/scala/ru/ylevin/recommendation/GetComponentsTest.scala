package ru.ylevin.recommendation

import org.scalatest.FunSuite

class GetComponentsTest extends FunSuite {
    test("Empty Test") {
        assertResult(List()) {
            Utils.getComponents(Array())
        }
    }

    test("Single Element Test") {
        assertResult(List(Set(0))) {
            Utils.getComponents(Array(Set()))
        }
    }

    test("Isolated Components Test") {
        assertResult(List(Set(0), Set(1), Set(2), Set(3))) {
            Utils.getComponents(Array(Set(), Set(), Set(), Set()))
        }
    }

    test("One Level Depth Test") {
        assertResult(List(Set(0, 1, 2), Set(3))) {
            Utils.getComponents(Array(Set(1, 2), Set(0), Set(0), Set()))
        }
    }

    test("Chain Test") {
        assertResult(List(Set(0, 1, 2, 3))) {
            Utils.getComponents(Array(Set(1), Set(2, 0), Set(1, 3), Set(2)))
        }
    }

    test("Cycle Test") {
        assertResult(List(Set(0, 1, 2, 3))) {
            Utils.getComponents(Array(Set(1), Set(0, 2, 3), Set(1, 3), Set(1, 2)))
        }
    }
}
