package ru.ylevin.recommendation

object FakeGeoGridSet extends GeoGridSet {
    private val defaultGrid = new FlatGeoGrid(
        latitudeMultiplier = 1.0,
        longitudeMultiplier = 1.0,
        gridSize = 1.0
    )

    override val gridCaseNum: Range = FlatGeoGrid.gridCaseNum

    override def gridForRegion(regionId: Int): GeoGrid = defaultGrid
}
